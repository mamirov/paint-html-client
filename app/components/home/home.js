angular.module('painter', ['restangular', 'ngRoute', 'ngSanitize']).
    config(function($routeProvider, RestangularProvider) {
        var viewsDir = 'vk/app/components/home/views/';
        $routeProvider.
            when('/list', {
                controller:Painter,
                templateUrl: viewsDir + 'painter.html'
            })
            .when('/album', {
                controller:Album,
                templateUrl: viewsDir + 'album.html'
            })
            .when('/start', {
                controller:StartGame,
                templateUrl: viewsDir + 'start.html'
            })
            .when('/', {
                controller:Main,
                templateUrl: viewsDir + 'main.html'
            });
        RestangularProvider.setBaseUrl('/api/v1/');
    });

function Painter($scope, $sce, Restangular, $location) {
    initPainter(Restangular, $scope, $location);
    $('#open-target-photo').click(function() {
        $scope.modal.title = "Натура";
        var htmlPhoto = '<img src="' + $scope.target.user.photo_max_orig + '">';
        $scope.modal.body = $sce.trustAsHtml(htmlPhoto);
        $scope.$apply();
        $('#myModal').modal('toggle');
    });
    console.log($scope.targetUser);
    console.log($scope.friends);
}

function StartGame($scope, Restangular, $location) {
    $(function(){
        $('#start-game').click(function() {
            var startButton = $(this);
            startButton.toggleClass('active');
            startButton.prop('disabled', true);
            Restangular.one('game').get().then(function(data) {
                if(data.error != undefined) {
                    $scope.modal.title = "Ошибка";
                    $scope.modal.body = data.message;
                    startButton.prop('disabled', false);
                    startButton.removeClass('active');
                    $('#myModal').modal('toggle');
                } else {
                    VK.api("users.get", {user_ids: data.id, fields : "photo_200,photo_max,photo_max_orig,domain"}, function(vkData) {
                        console.log(vkData.response[0]);
                        $scope.target.user = Restangular.copy(vkData.response[0]);
                        $scope.$apply();
                        $location.path('list');
                    });
                }
            });
        });
    });
}

function Album ($scope, Restangular) {
    Restangular.all('users/mypictures').getList().then(function(pictures) {
        $scope.pictures = pictures;
    });
}

function Main($scope, Restangular) {

}

function MainCtrl($interval, $scope, Restangular) {
    $scope.modal = {};
    $scope.target = {};
    VK.init(function() {
        VK.api("users.get", {fields : "domain"}, function(data) {
            Restangular.one("login").post("", data.response[0]);
        });
    }, function() {
        alert("Это не вк приложение, зайдите через вк");
    }, '5.40');

    $scope.users = [];
    $scope.users.route = "users";
    VK.api("users.get", {fields : "photo_50,domain"}, function(data) {
        $scope.users = Restangular.copy(data.response);
        $scope.$apply();
    });

    Restangular.one('users/info').get().then(function(current_user){
        $scope.currentUser = current_user;
        $scope.currentUser.energyWidth = current_user.energy/30*100;
    });
    $interval(function() {
        Restangular.one('users/info').get().then(function(current_user){
            $scope.currentUser = current_user;
            $scope.currentUser.energyWidth = current_user.energy/30*100;
        });
    }, 7000);

    VK.api("friends.get", {order: "random", count: 10, fields : "photo_50", name_case: "nom"}, function(data) {
        $scope.friends = Restangular.copy(data.response.items);
        $scope.$apply();
        $(function(){
            var currentCssValue = 0;
            var rightCssValue   = 16.66666667;

            friend = $('.friend');
            friend = $(friend);

            $('#next-friend, #preview-friend').click(function() {

                if($(this).attr('id') == 'preview-friend') {
                    currentCssValue -= rightCssValue;
                } else {
                    currentCssValue += rightCssValue;
                }

                friend.animate({
                    right : currentCssValue+'%'
                }, 100, function() {

                });
            });
        });
    });
}

function initPainter(Restangular, $scope, $location) {
    var $ = function(id){return document.getElementById(id)};

    var workSpace = $('workspace');

    var canvas = this.__canvas = new fabric.Canvas('c', {
        width: workSpace.offsetWidth - 5,
        height: workSpace.offsetHeight - 5,
        backgroundColor: "white",
        isDrawingMode: true
    });

    fabric.Object.prototype.transparentCorners = false;
    var drawingLineWidthEl = $('line-width');
    var drawingLineColorEl = $('line-color');
    var cancelButton = $('cancel-draw');
    var clearButton = $('clear-draw');
    var uploadImg = $('upload-image');

    canvas.freeDrawingBrush.width = 30;

    drawingLineWidthEl.onchange = function() {
        canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
        this.previousSibling.innerHTML = this.value;
    };

    drawingLineColorEl.onchange = function() {
        canvas.freeDrawingBrush.color = this.value;
    };

    clearButton.onclick = function() {
        canvas.clear();
    };

    cancelButton.onclick = function() {
        var lastItemIndex = (canvas.getObjects().length - 1);
        var item = canvas.item(lastItemIndex);

        if(item.get('type') === 'path') {
            canvas.remove(item);
            canvas.renderAll();
        }
    };

    uploadImg.onclick = function() {
        var fd = new FormData();
        var myBlob = new Blob([canvas.toDataURL('png')],{type:'image/png'});
        fd.append("name", "picture");
        fd.append("targetUser", $scope.target.user.id);
        fd.append("file", myBlob);
        Restangular.one('game/upload')
            .withHttpConfig({transformRequest: angular.identity})
            .customPOST(fd, '', undefined, {'Content-Type': undefined}).then(function(uploadResponse) {
                $scope.modal.title = uploadResponse.status == "success" ? "Успешно" : "Ошибка";
                $scope.modal.body = uploadResponse.message;
                $location.path('start');
            });
        if($scope.currentUser.energy >= 5) {
            $scope.currentUser.energy = $scope.currentUser.energy - 5;
            $scope.currentUser.energyWidth =  $scope.currentUser.energy/30*100;
        }
        $scope.$apply();
        canvas.clear();
    };
}